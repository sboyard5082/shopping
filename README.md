# Introduction 
Shopping is an app to organize different shopping list you have. You can have a groery list, a hardware list, a pet store list etc. 
The items inside the lists and the lists themselves can be reorganized by drag and drop
# Getting Started
1.  Clone the repository
2.	Download the latest version of Android Studio
3.	Build the application

# Contribute
Any improvements or additional features are welcome.  

# Note
The license was chosen because it is the most commonly used by Android projects. 